#pragma strict

public var hitSound : AudioClip;

function OnCollisionEnter(col : Collision){
	if(col.gameObject.tag != "enemy"){
		
		if(col.gameObject.tag == "Player" || col.gameObject.tag == "fist"){
			AudioSource.PlayClipAtPoint(hitSound,this.transform.position);
		}
		Destroy(this.gameObject);
	}
}
/*
function OnTriggerEnter(col : Collision){
	if(col.gameObject.tag != "enemy"){
		Destroy(this.gameObject);
	}
}*/