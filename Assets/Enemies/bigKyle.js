#pragma strict

public var chased : Transform;
public var speed : double = .5;
public var projectile : GameObject;
public var minFireRate : double;
public var maxFireRate : double;
public var damagedSound : AudioClip;
public var deathSound : AudioClip;
private var fireRate : double;
private var nextFireTime : double;
private var livesLeft : int = 10;

function Start () {
	var fireRateRange = maxFireRate - minFireRate;
	fireRate = minFireRate + Random.value * fireRateRange;
	nextFireTime = 0;
}

function Update () {
	//transform.rigidbody.velocity = (chased.position - transform.position).normalized * speed;
	var chaseVector = (chased.position - transform.position).normalized * speed;
	
	// set y to 0 so chaser doesnt hop around
	chaseVector.y = 0;
	
	rigidbody.MovePosition(rigidbody.position + chaseVector * Time.deltaTime);
	
	var chasedPostition = new Vector3(chased.position.x,this.transform.position.y,chased.position.z);
	this.transform.LookAt(chasedPostition);
	
	if(Time.time > nextFireTime){
		shoot();
		nextFireTime = Time.time + fireRate;
	}
}

function OnCollisionEnter(col: Collision){
	
	if(col.gameObject.tag == "fist"){
		livesLeft--;
		if(livesLeft>0){
			AudioSource.PlayClipAtPoint(damagedSound,this.transform.position);
		}
		else{
			AudioSource.PlayClipAtPoint(deathSound,this.transform.position);
			Destroy(this.gameObject);
			chased.SendMessage("killEnemy");
		}		
	}
}

function shoot(){
	var cloneProjectile : GameObject = Instantiate (projectile, projectile.transform.position, this.transform.rotation); 
	cloneProjectile.active = true; 

	cloneProjectile.transform.LookAt(chased);
	cloneProjectile.rigidbody.AddForce(cloneProjectile.transform.forward * 10000); 
}

