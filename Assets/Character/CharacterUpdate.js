#pragma strict

public var maxLives : int = 3;
private var lifeText : String;
private var enemiesText : String;
private var lives : int;
private var currentLevel = 0;
public var enemiesRemaining : int;
public var leftFist : GameObject;
public var rightFist : GameObject;
private var deathString = "";

function Start(){
	lives = maxLives;
	lifeText = "Lives: " + lives;
	
	//returns number of enemies that are in the current level						
	enemiesRemaining = numEnemies();
	enemiesText = "Enemies: " + enemiesRemaining;
}

function Update(){
	if(Input.GetMouseButtonDown(0)){
		moveFist(true);
	} else if (Input.GetMouseButtonDown(1)){
		moveFist(false);
	}
	
	if (lives <= 0 ){
		death();
	}
	
	if(enemiesRemaining <= 0){
		nextLevel();
	}
}



function OnCollisionEnter(col : Collision){
	if(col.gameObject.tag == "projectile"){
		loseLife();
	}
}

function OnGUI(){
	GUI.Label(Rect(10,10,140,20),lifeText);	
	GUI.Label(Rect(10,30,140,20),enemiesText);
	print(deathString);
	var deathStyle : GUIStyle = new GUIStyle();
	deathStyle.fontSize = 50;
	GUI.Label(Rect(750,500,300,50),deathString,deathStyle);
}

function killEnemy(){
	enemiesRemaining--;
	enemiesText = "Enemies: " + enemiesRemaining;
}

function moveFist(isLeft : boolean){
	if(isLeft){
		leftFist.SendMessage("setForward");
	} else {
		rightFist.SendMessage("setForward");
	}
}

function death(){
	deathString = "You Died";
	yield WaitForSeconds(1);
	Application.LoadLevel(Application.loadedLevel);
}

function loseLife(){
	lives--;
	lifeText = "Lives: " + lives;
}

function numEnemies(){
	
	//returns index of loadedLevel. index can be changed in build settings
	var level = Application.loadedLevel;
	
	switch (level){
		case 0:
		return 10;
		case 1:
		return 2;
		case 2:
		return 7;
		case 3:
		return 1;
		default:
		return 0;
	}
}

function nextLevel(){
	print(Application.loadedLevel);
	if(Application.loadedLevel == 3){
		deathString = "WINNER";
		yield WaitForSeconds(5);
	}
	else{
		//using deathstring because it's already there
		deathString = "Next Level";
		yield WaitForSeconds(1);
		Application.LoadLevel(Application.loadedLevel + 1);
	}
}