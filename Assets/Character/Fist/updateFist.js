#pragma strict

//the amount of time half the punch takes to execute
public var punchTime : float = .25;
public var punchSpeed : float = 5;
public var player : GameObject;

enum fistStates {idle,forward,backward};
private var state : fistStates = fistStates.idle;
private var startPosition : Vector3;

private var timer : float;

function Start(){
	timer = 0;
	startPosition = this.transform.localPosition;
}

function Update () {
	
	timer -= Time.deltaTime;
	
	switch (state){
		case fistStates.forward:
			// move the fist forward
			this.transform.Translate(Vector3.forward*Time.deltaTime*punchSpeed,Space.Self);
			
			//transition to moving backward after timer has counted down
			if(timer < 0){
				state = fistStates.backward;
				timer = punchTime;
			}			
		break;
		
		case fistStates.backward:
			
			//move the fist backward
			this.transform.Translate(Vector3.back*Time.deltaTime*punchSpeed,Space.Self);
			
			//transition to idle after timer has counted down
			if(timer < 0){
				state = fistStates.idle;
			}
		break;
		
		case fistStates.idle:
			this.transform.localPosition = startPosition;
		break;
	}
	
	
	
}

function OnCollisionEnter(col : Collision){
	if (col.gameObject.tag == "projectile" && state == fistStates.idle){
		player.SendMessage("loseLife");
	}
}
	

public function setForward(){	
	if(timer < 0){
		state = fistStates.forward;
		timer = punchTime;
	}	
}