#pragma strict


public var maxLives : int = 3;
private var lifeText : String;
private var enemiesText : String;
private var lives : int;
private var currentLevel = 0;
public var enemiesRemaining : int;
public var leftFist : GameObject;
public var rightFist : GameObject;
private var deathString = "";

//variables used by hydra
public var walkSpeed : float = 10.0;
public var rotationSpeed : float = 2.0;

private var plugin: RazerHydraPlugin = new RazerHydraPlugin();

public var myCamera :  GameObject;
public var myController : CharacterController;

private var prevLeft : Vector3 = Vector3(0,0,0);
private var prevRight : Vector3 = Vector3(0,0,0);
//end hydra variables


function Start(){
	plugin.init();
	
	lives = maxLives;
	lifeText = "Lives: " + lives;
	
	//returns number of enemies that are in the current level						
	enemiesRemaining = numEnemies();
	enemiesText = "Enemies: " + enemiesRemaining;
}

function Update(){
	
	//get data from left hydra
	plugin.getNewestData(0);
	var currentLeft : Vector3 = Vector3(plugin.data.position.x,plugin.data.position.y,plugin.data.position.z);	
	//move character according to left joystick
	myController.SimpleMove(myCamera.transform.forward * plugin.data.joystick_y * walkSpeed);
	myController.SimpleMove(myCamera.transform.right * plugin.data.joystick_x * walkSpeed);
	
	
	//get data from right hydra
	plugin.getNewestData(1);
	var currentRight : Vector3 = Vector3(plugin.data.position.x,plugin.data.position.y,plugin.data.position.z);
	
		
	// update the camera according to right joystick	
	var cameraX = myCamera.transform.rotation.eulerAngles.x - plugin.data.joystick_y * rotationSpeed;
	var cameraY = myCamera.transform.rotation.eulerAngles.y + plugin.data.joystick_x * rotationSpeed;
	
	print(plugin.data.joystick_x + ',' + plugin.data.joystick_y);
	var cameraZ = 0;
	
	// camera x-angle gets stuck if outside the 90-270 range, so adjust to safe range
	if(cameraX < 275 && cameraX > 255){
		cameraX = 275;
	}
	else if(cameraX > 85 && cameraX < 100){
		cameraX = 85;
	}
	
	// set camera angle
	myCamera.transform.rotation.eulerAngles = Vector3(cameraX,cameraY,cameraZ);
	
	//print(Vector3.Distance(currentRight,prevRight));
	//print(currentRight.ToString() + "   ,   " + currentLeft.ToString());
	if (Vector3.Distance(currentLeft,prevLeft)>40){
		moveFist(true);
	}
	
	
	if (Vector3.Distance(currentRight,prevRight)>40){
		
		moveFist(false);
	}
	
	prevLeft = currentLeft;
	prevRight = currentRight;
	  
	//print(currentLeftZ + "," + currentRightZ);
	
	if (lives <= 0 ){
		death();
	}
	
	if(enemiesRemaining <= 0){
		nextLevel();
	}
}



function OnCollisionEnter(col : Collision){
	if(col.gameObject.tag == "projectile"){
		loseLife();
	}
}

function OnGUI(){
	GUI.Label(Rect(10,10,140,20),lifeText);	
	GUI.Label(Rect(10,30,140,20),enemiesText);
	
	var deathStyle : GUIStyle = new GUIStyle();
	deathStyle.fontSize = 50;
	GUI.Label(Rect(750,500,300,50),deathString,deathStyle);
}

function killEnemy(){
	enemiesRemaining--;
	enemiesText = "Enemies: " + enemiesRemaining;
}

function moveFist(isLeft : boolean){
	if(isLeft){
		leftFist.SendMessage("setForward");
	} else {
		rightFist.SendMessage("setForward");
	}
}

function death(){
	deathString = "You Died";
	yield WaitForSeconds(1);
	Application.LoadLevel(Application.loadedLevel);
}

function loseLife(){
	lives--;
	lifeText = "Lives: " + lives;
}

function numEnemies(){
	
	//returns index of loadedLevel. index can be changed in build settings
	var level = Application.loadedLevel;
	
	switch (level){
		case 0:
		return 10;
		case 1:
		return 2;
		case 2:
		return 7;
		case 3:
		return 1;
		default:
		return 0;
	}
}

function nextLevel(){
	if(Application.loadedLevel == 3){
		deathString = "WINNER";
		yield WaitForSeconds(5);
	}
	else{
		//using deathstring because it's already there
		deathString = "Next Level";
		yield WaitForSeconds(1);
		Application.LoadLevel(Application.loadedLevel + 1);
	}
}